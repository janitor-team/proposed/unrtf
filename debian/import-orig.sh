#!/bin/bash
git-import-orig  \
     --filter=Makefile         \
     --filter=autom4te.cache/* \
     --filter=config.h         \
     --filter=config.log       \
     --filter=config.status    \
     --filter=doc/Makefile     \
     --filter=outputs/Makefile \
     --filter=patches/Makefile \
     --filter=src/.deps/*.Po   \
     --filter=src/Makefile     \
     --filter=stamp-h1         \
     --filter=tests/Makefile   \
     --filter=tests/.~lock.*#  \
     --filter=INSTALL          \
     --filter=Makefile.in      \
     --filter=aclocal.m4       \
     --filter=configure        \
     --filter=doc/Makefile.in  \
     --filter=outputs/Makefile.in  \
     --filter=patches/Makefile.in  \
     --filter=src/Makefile.in  \
     --filter=tests/Makefile.in  \
     $*
